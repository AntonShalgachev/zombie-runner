﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
	public Text healthText;
	public string healthTextString;

	Health playerHealth;
	Slider slider;

	float maxHealth;

	private void Awake()
	{
		slider = GetComponent<Slider>();
	}

	private void Update()
	{
		if (playerHealth)
			SetHp(playerHealth.GetHealth());
		else
			SetHp(0.0f);
	}

	void SetHp(float health)
	{
		var ratio = health / maxHealth;
		slider.value = ratio;

		healthText.text = string.Format(healthTextString, (int)health, (int)maxHealth);
	}

	public void SetPlayer(GameObject player)
	{
		playerHealth = player.GetComponent<Health>();
		maxHealth = playerHealth.GetMaxHealth();
	}
}
