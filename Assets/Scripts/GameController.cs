﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
	public GameObject dungeon;
	public GameObject playerPrefab;
	public GameObject zombiePrefab;
	public GameObject treasurePrefab;
	public HealthBar healthBar;
	public MeshRenderer fogOfWar;

	public HudController hud;

	DungeonGenerator dungeonGenerator;
	TileGenerator tileGenerator;
	GameObject player;
	PlayerController playerController;
	List<ZombieMovementAI> zombies;
	BackgroundMusic backgroundMusic;

	DungeonGenerator.DungeonData dungeonData;

	bool treasureCollected;

	public static GameController instance;

	static int seed = 0;

	private void Awake()
	{
		Debug.Assert(instance == null, "There can't be multiple game controllers");
		instance = this;

		this.zombies = new List<ZombieMovementAI>();
		backgroundMusic = FindObjectOfType<BackgroundMusic>();
		Debug.Assert(backgroundMusic);

		Debug.Assert(hud);
	}

	private void Start()
	{
		SetupGame();
	}

	private void Update()
	{
		if (playerController)
		{
			fogOfWar.material.SetVector("_PlayerPos", playerController.transform.position);
			fogOfWar.material.SetVector("_PlayerDir", playerController.transform.rotation * Vector2.right);
		}
	}

	void SetupGame()
	{
		dungeonGenerator = dungeon.GetComponent<DungeonGenerator>();
		tileGenerator = dungeon.GetComponent<TileGenerator>();

		dungeonGenerator.randomSeed = seed;
		seed++;

		dungeonData = dungeonGenerator.GenerateDungeon();

		tileGenerator.GenerateDungeon();

		SpawnPlayer();
		SpawnZombies();
		SpawnTreasure();

		hud.InitObjectives();
	}

	void SpawnPlayer()
	{
		var playerPos = tileGenerator.GetPlayerSpawn().transform.position;
		player = Instantiate(playerPrefab, playerPos, Quaternion.identity);

		Camera.main.GetComponent<PlayerFollower>().SetPlayer(player);
		healthBar.SetPlayer(player);

		playerController = player.GetComponent<PlayerController>();
		Debug.Assert(playerController);
		SetupPlayer();
	}

	void SpawnZombies()
	{
		var spawns = tileGenerator.GetZombieSpawns();
		var zombies = new GameObject("Zombies");

		foreach (var spawn in spawns)
		{
			var pos = spawn.transform.position;
			var zombie = Instantiate(zombiePrefab, pos, Quaternion.identity, zombies.transform);
			zombie.name = "Zombie";

			this.zombies.Add(zombie.GetComponent<ZombieMovementAI>());
		}
	}

	void SpawnTreasure()
	{
		var pos = tileGenerator.GetTreasureSpawn().transform.position;
		var player = Instantiate(treasurePrefab, pos, Quaternion.identity);
	}

	void SetupPlayer()
	{
		playerController.onTreasureCollected += OnTreasureCollected;
		playerController.onSpawnReached += OnSpawnReached;
		playerController.onDeath += OnDeath;
	}

	void OnTreasureCollected()
	{
		Debug.Log("Treasure collected");
		treasureCollected = true;
		hud.OnTreasureCollected();

		foreach (var zombie in zombies)
		{
			zombie.ForceVisiblePlayer(player);
		}

		backgroundMusic.StartPlayingIntenseClip();
	}

	void OnSpawnReached()
	{
		if (!treasureCollected)
			return;

		Debug.Log("Spawn reached");
		hud.OnSpawnReached();
		OnWin();
	}

	void OnDeath()
	{
		if (fogOfWar)
			fogOfWar.gameObject.SetActive(false);

		hud.OnDeath();

		Invoke("GotoMenu", 5.0f);
	}

	void OnWin()
	{
		fogOfWar.gameObject.SetActive(false);
		hud.OnWin();

		playerController.OnWin();
		foreach (var zombie in zombies)
		{
			zombie.OnWin();
		}

		Invoke("GotoMenu", 5.0f);
	}

	void GotoMenu()
	{
		SceneManager.LoadScene("Start");
	}
}
