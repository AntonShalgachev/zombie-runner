﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusic : MonoBehaviour
{
	public AudioClip[] clips;
	public AudioClip intenseClip;

	AudioSource source;

	public static BackgroundMusic instance;

	private void Awake()
	{
		if (instance == null)
		{
			DontDestroyOnLoad(this);
			instance = this;
			source = GetComponent<AudioSource>();
		}
		else
		{
			DestroyImmediate(this);
		}
	}

	public void StartPlayingRandomClip()
	{
		CancelInvoke();

		var i = Random.Range(0, clips.Length);
		var clip = clips[i];

		source.Stop();
		source.PlayOneShot(clip);

		Invoke("StartPlayingRandomClip", clip.length);
	}

	public void StartPlayingIntenseClip()
	{
		CancelInvoke();

		source.Stop();
		source.PlayOneShot(intenseClip);

		Invoke("StartPlayingIntenseClip", intenseClip.length);
	}
}
