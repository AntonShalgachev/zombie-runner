﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieMovementAI : MonoBehaviour
{
	public float wanderingSpeedMultiplier;
	public float wanderingNoiseScale;
	public float visibleRange;
	public float visibleRangeExtra;
	public LayerMask playerLayerMask;
	public AudioClip[] groans;

	GameObject playerInRange;
	GameObject visiblePlayer;
	bool forcedVisiblePlayer = false;
	bool won = false;
	Movement movement;

	float wanderingXOffset;
	float wanderingYOffset;

	private void Awake()
	{
		movement = GetComponent<Movement>();

		wanderingXOffset = Random.Range(-10000, 10000);
		wanderingYOffset = Random.Range(-10000, 10000);
	}

	private void Start()
	{
		InvokeRepeating("UpdateVisiblePlayer", 0.0f, 0.1f);
	}

	//void ScheduleNextGroan()
	//{
	//	var delay = Random.Range(1.0f, 3.0f);

	//	Invoke("TryPlayGroanSound", delay);

	//	if (visiblePlayer)
	//		Invoke("ScheduleNextGroan", delay);
	//}

	//void StopAllGroans()
	//{
	//	foreach (var source in GetComponents<AudioSource>())
	//	{
	//		source.Stop();
	//	}
	//}

	//void TryPlayGroanSound()
	//{
	//	if (visiblePlayer)
	//		PlayGroanSound();
	//}

	void UpdateVisiblePlayer()
	{
		if (!playerInRange)
		{
			var visibleCollider = Physics2D.OverlapCircle(transform.position, visibleRange, playerLayerMask);
			if (visibleCollider)
			{
				playerInRange = visibleCollider.gameObject;
				//ScheduleNextGroan();
			}
		}
		else
		{
			var visibleCollider = Physics2D.OverlapCircle(transform.position, visibleRangeExtra, playerLayerMask);
			if (!visibleCollider)
			{
				playerInRange = null;
			}
		}
	}

	private void Update()
	{
		if (!forcedVisiblePlayer)
		{
			visiblePlayer = null;
			if (playerInRange)
			{
				var direction = playerInRange.transform.position - transform.position;
				var dist = direction.magnitude;
				var layerMask = 1 << LayerMask.NameToLayer("Wall");
				var wallHit = Physics2D.Raycast(transform.position, direction.normalized, dist, layerMask);
				if (wallHit.collider == null)
					visiblePlayer = playerInRange;
			}
		}

		var dir = Vector2.zero;
		var speedMultiplier = 1.0f;
		if (!won && visiblePlayer)
		{
			dir = visiblePlayer.transform.position - transform.position;
		}
		else
		{
			dir = GetWanderingDirection();
			speedMultiplier = wanderingSpeedMultiplier;
		}

		movement.SetDirection(dir);
		movement.SetVelocityMultiplier(speedMultiplier);
	}

	//void PlayGroanSound()
	//{
	//	var i = Random.Range(0, groans.Length);
	//	var clip = groans[i];

	//	var sources = GetComponents<AudioSource>();
	//	foreach (var source in sources)
	//	{
	//		if (!source.isPlaying)
	//		{
	//			source.PlayOneShot(clip);
	//			return;
	//		}
	//	}

	//	var newSource = gameObject.AddComponent<AudioSource>();
	//	newSource.PlayOneShot(clip);
	//}

	Vector2 GetWanderingDirection()
	{
		var t = Time.time * wanderingNoiseScale;

		var x = Mathf.PerlinNoise(t + wanderingXOffset, 0.0f);
		var y = Mathf.PerlinNoise(0.0f, t + wanderingYOffset);

		x = Mathf.Lerp(-1.0f, 1.0f, x);
		y = Mathf.Lerp(-1.0f, 1.0f, y);

		return new Vector2(x, y);
	}

	//private void OnTriggerEnter2D(Collider2D collider)
	//{
	//	var obj = collider.gameObject;
	//	if (obj.layer == LayerMask.NameToLayer("Player"))
	//	{
	//		playerInRange = obj;
	//	}
	//}

	//private void OnTriggerExit2D(Collider2D collider)
	//{
	//	var obj = collider.gameObject;
	//	if (obj.layer == LayerMask.NameToLayer("Player"))
	//	{
	//		playerInRange = null;
	//	}
	//}

	public void ForceVisiblePlayer(GameObject player)
	{
		visiblePlayer = player;
		forcedVisiblePlayer = true;
	}

	public void OnWin()
	{
		won = true;
	}
}
